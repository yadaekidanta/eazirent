<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewTable extends Migration
{
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->longText('comment');
            $table->string('rating');
            $table->timestamp('created_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('review');
    }
}
