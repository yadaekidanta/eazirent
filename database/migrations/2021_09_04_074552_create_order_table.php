<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_id');
            $table->integer('penyedia_id');
            $table->integer('user_id');
            $table->date('booking_at');
            $table->string('booking_time')->nullable();
            $table->string('days',4);
            $table->string('total',30,0);
            $table->enum('payment_type',['cod','tf']);
            $table->string('file')->nullable();
            $table->timestamp('take_at')->nullable();
            $table->timestamp('return_at')->nullable();
            $table->enum('st',['Wait for confirmation','Rent on going','Rent finish']);
            $table->timestamp('created_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
