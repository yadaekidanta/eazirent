<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleModelTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_model', function (Blueprint $table) {
            $table->id();
            $table->integer('brand_id');
            $table->string('name');
            $table->string('slug');
        });
    }
    public function down()
    {
        Schema::dropIfExists('vehicle_model');
    }
}