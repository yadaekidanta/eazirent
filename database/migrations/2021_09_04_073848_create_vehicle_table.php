<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('category_id');
            $table->integer('brand_id');
            $table->integer('vehicle_model_id');
            $table->longText('desc');
            $table->string('photo');
            $table->float('price',20,0);
            $table->enum('st',['open','used']);
            $table->timestamp('created_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('vehicle');
    }
}
