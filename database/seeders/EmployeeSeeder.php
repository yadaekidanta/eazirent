<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'name' => 'Administrator',
                'email' => 'admin@eazirent.com',
                'password' => Hash::make('password'),
                'email_verified_at' => date("Y-m-d H:i:s"),
            ],
        );
        foreach($data AS $d){
            Employee::create([
                'name' => $d['name'],
                'email' => $d['email'],
                'password' => $d['password'],
                'email_verified_at' => $d['email_verified_at']
            ]);
        }
    }
}
