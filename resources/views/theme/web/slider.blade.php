@php
    $banner= \App\Models\Banner::get();
@endphp
<section id="slider" class="slider-element swiper_wrapper min-vh-60 min-vh-md-100" data-dots="true" data-loop="true" data-grab="false">

    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
            @foreach ($banner as $item)
            <div class="swiper-slide dark">
                <div class="container">
                    <div class="slider-caption top-left">
                        <div>
                            <h2 class="font-primary nott" style="color: black;">{{$item->title}}</h2>
                            <p class="fw-light font-primary nott d-none d-sm-block" style="color: black;">{{$item->desc}}</p>
                            <a href="{{$item->url}}" class="button button-rounded button-border button-white button-light nott" style="color: black;">View Details</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide-bg" style="background-image: url('{{$item->image}}');"></div>
            </div>
            @endforeach
        </div>
        <div class="swiper-pagination"></div>
    </div>

</section>