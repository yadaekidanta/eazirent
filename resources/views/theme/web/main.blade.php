<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web.head')
<body class="stretched side-push-panel" data-loader-html="<div><img src='demos/car/images/page-loader.gif' alt='Loader'></div>">
	<!-- Side Panel Overlay -->
	<div class="body-overlay"></div>
	<!-- Side Panel -->
	<div id="side-panel">
		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">

			<div class="widget clearfix">

				<a href="index.html"><img src="demos/car/images/logo@2x.png" alt="Canvas Logo" height="50"></a>

				<p>It has always been, and will always be, about quality. We're passionate about ethically sourcing the finest coffee beans, roasting them with great care. We'd like to hear your message!</p>

				<div class="widget quick-contact-widget form-widget border-0 pt-0 clearfix">

					<h4>Quick Contact</h4>
					<div class="form-result"></div>
					<form id="form_input">
						<input type="text" class="required sm-form-control input-block-level" name="name" value="{{Auth::guard('member')->user()->name ?? ''}}" placeholder="Full Name" />
						<input type="text" class="required sm-form-control email input-block-level" name="email" value="{{Auth::guard('member')->user()->email ?? ''}}" placeholder="Email Address" />
						<textarea class="required sm-form-control input-block-level short-textarea"  name="message" rows="4" cols="30" placeholder="Message"></textarea>
						<button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.contact.store')}}','POST');" name="quick-contact-form-submit" class="button button-small button-3d m-0" value="submit">Send Email</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header ============================================= -->
		@include('theme.web.header')
		<!-- #header end -->
		<!-- Slider ============================================= -->
		@if (request()->is('/'))
		@include('theme.web.slider')
		@endif
		<!-- #Slider End -->
		<!-- Content ============================================= -->
		{{$slot}}
		<!-- #content end -->
		<!-- Footer ============================================= -->
		@include('theme.web.footer')
		<!-- #footer end -->
	</div>
	<!-- #wrapper end -->
	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	<!-- Contact Button ============================================= -->
	<div id="contact-me" class="icon-line-mail side-panel-trigger bg-color"></div>
	<!-- JavaScripts ============================================= -->
	@include('theme.web.js')
	@yield('custom_js')
</body>
</html>