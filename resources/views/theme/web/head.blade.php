<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Yada Ekidanta" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Stylesheets ============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Mukta+Vaani:300,400,500,600,700|Open+Sans:300,400,600,700,800,900&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
	<!-- Bootstrap Select CSS -->
	<link rel="stylesheet" href="{{asset('semicolon/css/components/bs-select.css')}}" type="text/css" />
	<!-- car Specific Stylesheet -->
	<link rel="stylesheet" href="{{asset('semicolon/demos/car/car.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/demos/car/css/car-icons/style.css')}}" type="text/css" />
	<!-- / -->
	<link rel="stylesheet" href="{{asset('semicolon/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/magnific-popup.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/demos/car/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="{{asset('semicolon/css/colors.php?color=c85e51')}}" type="text/css" />
	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/settings.css')}}" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/layers.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/navigation.css')}}">
	<link rel="stylesheet" href="{{asset('css/toastr.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/components/datepicker.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/components/timepicker.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/components/daterangepicker.css')}}" type="text/css" />
	<!-- Document Title ============================================= -->
	<title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
	<style>
		/* Revolution Slider */
		.ares .tp-tab { border: 1px solid #eee; }
		.ares .tp-tab-content { margin-top: -4px; }
		.ares .tp-tab-content { padding: 15px 15px 15px 110px; }
		.ares .tp-tab-image { width: 80px;height: 80px; }
	</style>
</head>