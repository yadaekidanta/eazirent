@php
function getAvatar()
{
    $photo = Auth::guard('member')->user()->photo ?? '';
    if($photo){
        return asset('storage/' . Auth::guard('member')->user()->photo);
    }
    else{
        return asset('img/avatars/5m.png');
    }
}
@endphp
<header id="header" class="full-header header-size-custom" data-sticky-shrink="false">
    <div id="header-wrap">
        <div class="container-fluid">
            <div class="header-row flex-lg-row-reverse">
                <!-- Logo ============================================= -->
                <div id="logo" class="me-lg-0 ms-lg-auto">
                    <a href="{{route('web.home')}}" class="standard-logo">
                        <img src="{{asset('semicolon/demos/car/images/logo.png')}}" alt="{{config('app.name')}}">
                    </a>
                    <a href="{{route('web.home')}}" class="retina-logo">
                        <img src="{{asset('semicolon/demos/car/images/logo@2x.png')}}" alt="{{config('app.name')}}">
                    </a>
                </div>
                <!-- #logo end -->
                <div id="primary-menu-trigger">
                    <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                </div>
                <!-- Primary Navigation ============================================= -->
                <nav class="primary-menu with-arrows">
                    <ul class="menu-container">
                        <li class="menu-item current">
                            <a class="menu-link" href="{{route('web.home')}}">
                                <div>Home</div>
                            </a>
                        </li>
                        <!-- Mega Menu -->
                        <li class="menu-item mega-menu">
                            <a class="menu-link" href="demo-car-single.html">
                                <div>Models</div>
                            </a>
                            <div class="mega-menu-content mega-menu-style-2">
                                <div class="container">
                                    <div class="row">
                                        <ul class="sub-menu-container mega-menu-column col-12">
                                            <li class="menu-item">
                                                <div class="widget text-center">
                                                    <h3 class="mb-0">Rental unggulan</h3>
                                                    <a href="#" class="button button-small button-rounded button-border button-dark button-black font-primary" style="margin: 10px 0 40px">Show all Cars</a>
                                                    <!-- Mega Menu Cars Carousel -->
                                                    <div class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="false" data-pagi="true" data-items-xs="1" data-items-sm="2" data-items-md="4" data-items-lg="6" data-items-xl="6">
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/1.png" alt="Car">BMW 1 Series ActiveE</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/4.png" alt="Car">Mercedes-Benz S-Class</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/5.png" alt="Car">Gran Turismo</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/6.png" alt="Car">Mclaren 675LT SPIDER</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/7.png" alt="Car">Honda City</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/8.png" alt="Car">Toyota Qualis</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/9.png" alt="Car">Honda WR-V</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/10.png" alt="Car">Suzuki Breeza</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/2.png" alt="Car">Chevrolet Spark</a></div>
                                                        <div class="oc-item center"><a href="demo-car-single.html"><img src="demos/car/images/mega-menu/3.png" alt="Car">Honda JaZZ</a></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="menu-item"><a class="menu-link" href="{{route('web.vehicle.index')}}"><div>Daftar Kendaraan</div></a></li>
                        @if(!Auth::guard('member')->check())
                        <li class="menu-item"><a class="menu-link" href="{{route('web.auth.index')}}"><div>Masuk / Daftar</div></a></li>
                        @else
                        <li class="menu-item"><a class="menu-link" href="{{route('web.order.show')}}"><div>Transaksi</div></a></li>
                        <div class="dropdown mx-3 me-lg-0">
                            <a href="javascript:;" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <img src="{{ getAvatar()}}"
                                    class="img-circle img-thumbnail" style="width: 30px; height: 30px; object-fit: cover;">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item text-start" href="{{route('web.profile.index')}}">Profil Saya</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-start" href="{{route('web.auth.logout')}}">Keluar <i class="icon-signout"></i></a>
                            </ul>
                        </div>
                        @endif
                    </ul>
                </nav>
                <!-- #primary-menu end -->
            </div>
        </div>
    </div>
    <div class="header-wrap-clone"></div>
</header>