<div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
    <div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
        <span class="mr-1">2021©</span>
        <a href="http://yadaekidanta.com" target="_blank" class="text-dark-75 text-hover-primary">{{config('app.name')}}</a>
    </div>
</div>