<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->name}}</td>
            <td>
                <a href="javascript:;" onclick="handle_confirm('Delete Confirmation','Yes','Cancel','DELETE','{{route('office.employee.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-circle mr-2">
                    <i class="flaticon-file"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}