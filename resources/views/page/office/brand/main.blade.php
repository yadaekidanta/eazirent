<x-office-layout title="Brand">
    <div id="content_list">
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Brand</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <span class="text-muted font-weight-bold mr-4">List Data</span>
                    <a href="javascript:;" onclick="load_input('{{route('office.brand.create')}}');" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a>
                </div>
                <div class="d-flex align-items-center">
                    <form id="content_filter">
                        <input class="form-control" onkeyup="load_list(1);" name="keywords" placeholder="Search here...">
                    </form>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xxl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div id="list_result"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script type="text/javascript">
            load_list(1);
        </script>
    @endsection
</x-office-layout>