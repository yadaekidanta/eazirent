<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Alamat</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->phone}}</td>
            <td>{{$item->address}}</td>
            <td>
                <a title="Verified" data-bs-toggle="tooltip" name="email_verified_at" data-bs-placement="top" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.member.verif',$item->id)}}');" class="btn btn-icon btn-primary"><i class="bi bi-x-lg"></i>Verified</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}