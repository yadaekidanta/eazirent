<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Hari</th>
            <th>Total</th>
            <th>Tipe Pembayaran</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->days}}</td>
            <td>{{$item->total}}</td>
            <td>{{$item->payment_type}}</td>
            <td>{{$item->st}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('office.order.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-circle mr-2">
                    <i class="flaticon-file"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Delete Confirmation','Yes','Cancel','DELETE','{{route('office.order.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-circle mr-2">
                    <i class="flaticon-file"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}