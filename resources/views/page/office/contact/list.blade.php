<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Pesan</th>
            <th>Time</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->message}}</td>
            <td> {{ $item->created_at }}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('office.contact.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-circle mr-2">
                    <i class="flaticon-file"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Delete Confirmation','Yes','Cancel','DELETE','{{route('office.contact.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-circle mr-2">
                    <i class="flaticon-file"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}