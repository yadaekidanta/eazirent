
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-2">
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Banner</h5>
            <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            <span class="text-muted font-weight-bold mr-4">
                @if ($banner->id)
                    Update
                @else
                    Add
                @endif
                Data
            </span>
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-light-warning font-weight-bolder btn-sm">Back</a>
        </div>
        <div class="d-flex align-items-center">
            
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xxl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Judul:</label>
                                    <input type="text" value="{{$banner->title}}" class="form-control" name="title" placeholder="Masukkan Judul.."/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Deskripsi:</label>
                                    <input type="text" value="{{$banner->desc}}" class="form-control" name="desc" placeholder="Masukkan Deskripsi.."/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Url:</label>
                                    <input type="text" value="{{$banner->url}}" class="form-control" name="url" placeholder="Masukkan Url.."/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Photo:</label>
                                    <input type="file" value="{{$banner->photo}}" class="form-control" name="photo" placeholder="Masukkan Photo.."/>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($banner->id)
                                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.banner.update',$banner->id)}}','PATCH');" class="btn btn-primary mr-2">Save</button>
                                    @else
                                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.banner.store')}}','POST');" class="btn btn-primary mr-2">Save</button>
                                    @endif
                                    <button onclick="load_list(1);" class="btn btn-secondary">Cancel</button>
                                </div>
                                @if ($banner->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button type="reset" class="btn btn-danger">Delete</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
