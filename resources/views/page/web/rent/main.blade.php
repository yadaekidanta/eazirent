<x-web-layout title="Daftar Rental Kendaraan">
    <div id="content_list">
        <section id="content" class="clearfix">
            <div class="content-wrap py-0">
                <div class="section m-0">
                    <div class="container">
                        <div class="row py-5 clearfix">
                            <div class="col-md-4 mb-md-3 mb-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-battery"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>Long Battery Life</h3>
                                        <p>Canvas provides support for Native HTML5 Videos that can be added to a Background.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-md-3 mb-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-cogs2"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>24x7 Service</h3>
                                        <p>Complete control on each &amp; every element that provides endless customization.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-md-3 mb-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-pump"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>Petrol, Diesel &amp; LPG</h3>
                                        <p>Change your Website's Primary Scheme instantly by simply adding the dark class.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mt-0 mt-md-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-meter"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>Powerful Dashboard</h3>
                                        <p>Canvas provides support for Native HTML5 Videos that can be added to a Background.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mt-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-paint"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>Differnt Color Options</h3>
                                        <p>Complete control on each &amp; every element that provides endless customization.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mt-5">
                                <div class="feature-box fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-car-wheel"></i></a>
                                    </div>
                                    <div class="fbox-content">
                                        <h3>All wheel Drive</h3>
                                        <p>Change your Website's Primary Scheme instantly by simply adding the dark class.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix border-bottom align-content-stretch flex-wrap">
                    <a href="#" class="col-lg-3 image_fade col-sm-6 p-5 order-1" style="background: url('demos/car/images/dealers/car-wheel.jpg') no-repeat center center / cover;">
                        <div class="p-5 p-sm-0"></div>
                    </a>
                    <div class="col-lg-3 col-sm-6 arrow-box bg-white py-5 px-4 d-flex align-self-center order-2">
                        <div class="card border-0">
                            <div class="card-body">
                                <h3 class="mb-1">Car Alloy Wheels</h3>
                                <h5 class="text-muted fw-normal">With faded secondary text</h5>
                                <p class="mb-0 text-black-50">Monotonectally actualize error-free total linkage with competitive niches. Competently initiate 24/7 internal.</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="col-lg-3 col-sm-6 image_fade p-5 order-3 order-sm-4 order-lg-3" style="background: url('demos/car/images/dealers/car-light.jpg') no-repeat center center / cover;">
                        <div class="p-5 p-sm-0"></div>
                    </a>
                    <div class="col-lg-3 col-sm-6 arrow-box bg-white py-5 px-4 d-flex align-self-center order-4 order-sm-3 order-lg-4">
                        <div class="card border-0">
                            <div class="card-body">
                                <h3 class="mb-1">Car Head Lights</h3>
                                <h5 class="text-muted fw-normal">With faded secondary text</h5>
                                <p class="mb-0 text-black-50">Monotonectally actualize error-free total linkage with competitive niches. Competently initiate 24/7 internal.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 arrow-box right bg-white py-5 px-4 d-flex align-self-center order-6 order-md-6 order-lg-5">
                        <div class="card border-0">
                            <div class="card-body">
                                <h3 class="mb-1">Premium Interiors</h3>
                                <h5 class="text-muted fw-normal">With faded secondary text</h5>
                                <p class="mb-0 text-black-50">Monotonectally actualize error-free total linkage with competitive niches. Competently initiate 24/7 internal.</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="col-lg-3 image_fade col-sm-6 p-5 order-md-5 order-5 order-lg-6" style="background: url('demos/car/images/dealers/car-interior.jpg') no-repeat center center / cover;">
                        <div class="p-5 p-sm-0"></div>
                    </a>
                    <div class="col-lg-3 col-sm-6 arrow-box right bg-white py-5 px-4 d-flex align-self-center order-8 order-sm-7">
                        <div class="card border-0">
                            <div class="card-body">
                                <h3 class="mb-1">Comfortable Leather Seats</h3>
                                <h5 class="text-muted fw-normal">With faded secondary text</h5>
                                <p class="mb-0 text-black-50">Monotonectally actualize error-free total linkage with competitive niches. Competently initiate 24/7 internal.</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="col-lg-3 image_fade col-sm-6 p-5 order-7 order-sm-8" style="background: url('demos/car/images/dealers/car-seat.jpg') no-repeat center center / cover;">
                        <div class="p-5 p-sm-0"></div>
                    </a>
                </div>
                <div class="section m-0">
                    <div class="container">
                        <div class="heading-block mb-4 center">
                            <h2>Rekomendasi Rental Kendaraan</h2>
                        </div>
                        <!-- Portfolio Items ============================================= -->
                        <div id="portfolio" class="portfolio row grid-container gutter-20 col-mb-30" data-layout="fitRows">
                            <!-- Car 1 -->
                            @foreach ($collection as $item)
                            <article class="portfolio-item col-12 col-sm-6 col-md-4 cf-sedan">
                                <div class="grid-inner">
                                    <div class="portfolio-image">
                                        <a href="#">
                                            <img src="demos/car/images/filter-cars/1.jpg" alt="Open Imagination">
                                            <div class="filter-p-pricing">
                                                <span class="p-price fw-bold ls1">$32,568</span>
                                                <span class="p-price-msrp">MSRP : <strong>$35,698</strong></span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Ford Mustang - White</a></h3>
                                        <span>Dramatically synthesize parallel applications vis-a-vis revolutionary e-tailers. Monotonectally incubate cooperative partnerships.</span>
                                    </div>
                                    <div class="row g-0 car-p-features font-primary clearfix">
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-meter"></i><span> 20000</span></div>
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-door"></i><span> 5 Seater</span></div>
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-fuel2"></i><span> 20kmpl</span></div>
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-signal"></i><span> Automatic</span></div>
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-wheel"></i><span> 15 Inch</span></div>
                                        <div class="col-lg-4 col-6 p-0"><i class="icon-car-care"></i><span> 2021</span></div>
                                    </div>
                                </div>
                            </article> 
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Buy And Sell Section ============================================= -->
                <div class="section m-0 p-0 clearfix">
                    <div class="row align-items-stretch clearfix">
                        <!-- Half Section 1 -->
                        <div class="col-lg-6 dark bg-color" style="background: url('demos/car/images/5.jpg') center center no-repeat; background-size: cover;">
                            <div class="col-padding clearfix">
                                <i class="i-plain i-xlarge icon-car-service inline-block" style="margin-bottom: 20px;"></i>
                                <div class="heading-block border-0" style="margin-bottom: 20px;">
                                    <h3>Are You Looking for a New Car?</h3>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, aspernatur, doloribus. Aspernatur, maiores earum eaque quas temporibus eius dolore dicta.</p>
                                <a href="#" class="button button-rounded button-white button-light m-0">Know More</a>
                            </div>
                        </div>
                        <!-- Half Section 2 -->
                        <div class="col-lg-6 clearfix bg-color" style="background: url('demos/car/images/6.jpg') center center no-repeat; background-size: cover;">
                            <div class="col-padding clearfix">
                                <i class="i-plain i-xlarge icon-car-care inline-block" style="margin-bottom: 20px;"></i>
                                <div class="heading-block border-0" style="margin-bottom: 20px;">
                                    <h3>Want to sell a used car?</h3>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, aspernatur, doloribus. Aspernatur, maiores earum eaque quas temporibus eius dolore dicta.</p>
                                <a href="#" class="button button-large button-dark button-black button-rounded m-0">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</x-web-layout>