<x-web-layout title="Profil Saya">
    <div id="content_list">
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="row clearfix">
                        <div class="col-md-9">
                            <a href="javascript:;" onclick="load_input('{{route('web.profile.edit')}}');" class="button button-rounded button-small m-0 ms-2 float-end">Ubah Profil</a>
                            <img src="{{$profile->image}}" class="alignleft img-circle img-thumbnail my-0" alt="Avatar" style="max-width: 84px;">
                            <div class="heading-block border-0">
                                <h3>{{Auth::guard('member')->user()->name}}</h3>
                                <span>{{Auth::guard('member')->user()->bio}}</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="list-group">
                                <a href="{{route('web.profile.index')}}" class="{{request()->is('profile/*') ? 'active' : ''}} list-group-item list-group-item-action d-flex justify-content-between"><div>Profil</div><i class="icon-user"></i></a>
                                <a href="{{route('web.auth.logout')}}" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Keluar</div><i class="icon-line2-logout"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            // load_list(1);
        </script>
    @endsection
</x-web-layout>