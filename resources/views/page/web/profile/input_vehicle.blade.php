<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <a href="javascript:;" onclick="load_list(1);" class="button button-rounded button-small m-0 ms-2 float-end">Kembali</a>
                    <div class="heading-block border-0">
                        <h3>{{Auth::guard('member')->user()->name}}</h3>
                    </div>
                    <div class="clear"></div>
                    <h5 class="me-3 mt-3">Tambah Kendaraan</h5>
                    <div class="card">
                        <div class="card-body">
                            <form id="form_basic_info">
                                <div class="form-group row">
                                    <div class="col-3">
                                        <label>Nama</label>
                                        <input type="text" name="name" class="form-control" placeholder="Full name" value="{{Auth::guard('member')->user()->name}}" />
                                    </div>
                                    <div class="col-3">
                                        <label>Brand:</label>
                                        <select data-control="select2" data-placeholder="Pilih Brand" id="brand_id" name="brand_id" class="form-control">
                                            <option SELECTED DISABLED>Pilih Brand</option>
                                            @foreach ($brand as $item)
                                                <option value="{{$item->id}}"{{$vehicle->brand_id==$item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <label>Category:</label>
                                        <select data-control="select2" data-placeholder="Pilih Category" id="category_id" name="category_id" class="form-control">
                                            <option SELECTED DISABLED>Pilih Category</option>
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}"{{$vehicle->category_id==$item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <label>Vehicle Model:</label>
                                        <select data-control="select2" data-placeholder="Pilih Category" id="vehicle_model_id" name="vehicle_model_id" class="form-control">
                                            <option SELECTED DISABLED>Pilih Vehicle-model</option>
                                            @foreach ($vehicleModel as $item)
                                                <option value="{{$item->id}}"{{$vehicle->vehicle_model_id==$item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <label>Deskripsi</label>
                                        <textarea name="desc" class="form-control"></textarea>
                                    </div>
                                    <div class="col-6">
                                        <label>Foto</label>
                                        <input type="file" class="form-control" name="photo">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label>Harga:</label>
                                        <input type="text" value="{{number_format($vehicle->price)}}" id="price" class="form-control" name="price" placeholder="Masukkan Harga.."/>
                                    </div>
                                </div> 
                                @if ($vehicle->id)
                                <button id="tombol_simpan_basic" onclick="handle_upload('#tombol_simpan_basic','#form_basic_info','{{route('web.vehicle.update',$item->id)}}','PATCH')" class="button button-rounded button-small m-0 ms-2 float-end">Simpan Perubahan</button>
                                @else
                                <button id="tombol_simpan_basic" onclick="handle_upload('#tombol_simpan_basic','#form_basic_info','{{route('web.vehicle.store')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Simpan Perubahan</button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    number_only('price');
    ribuan('price');
</script>