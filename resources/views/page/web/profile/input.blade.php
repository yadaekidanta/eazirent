<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <a href="javascript:;" onclick="load_list(1);" class="button button-rounded button-small m-0 ms-2 float-end">Kembali</a>
                    <div class="heading-block border-0">
                        <h3>{{Auth::guard('member')->user()->name}}</h3>
                    </div>
                    <div class="clear"></div>
                    <h5 class="me-3 mt-3">Informasi Dasar</h5>
                    <div class="card">
                        <div class="card-body">
                            <form id="form_basic_info">
                                <div class="form-group row">
                                    <div class="col-4">
                                        <label>Nama</label>
                                        <input type="text" name="name" class="form-control" placeholder="Full name" value="{{Auth::guard('member')->user()->name}}" />
                                    </div>
                                    <div class="col-4">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" value="{{Auth::guard('member')->user()->email}}" />
                                    </div>
                                    <div class="col-3">
                                        <label>NIK</label>
                                        <input type="tel" id="nik" name="nik" minlength="16" maxlength="16" class="form-control" value="{{Auth::guard('member')->user()->nik}}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <label>Alamat</label>
                                        <textarea name="address" class="form-control"></textarea>
                                    </div>
                                    <div class="col-6">
                                        <label>Foto KTP</label>
                                        <input type="file" class="form-control" name="ktp">
                                    </div>
                                </div>
                                <button id="tombol_simpan_basic" onclick="handle_upload('#tombol_simpan_basic','#form_basic_info','{{route('web.profile.update')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Simpan Perubahan</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-3">
                    <h5 class="me-3 mt-3">Keamanan Akun</h5>
                    <div class="card">
                        <div class="card-body">
                            <form id="form_password">
                                <div class="form-group row">
                                    <div class="col-4">
                                        <label>Kata Sandi Saat ini</label>
                                        <input type="password" class="form-control" name="current_password" id="current_password" />
                                    </div>
                                    <div class="col-4">
                                        <label>Kata Sandi</label>
                                        <input type="password" class="form-control" name="new_password" id="new_password" />
                                    </div>
                                    <div class="col-4">
                                        <label>Konfirmasi Kata Sandi</label>
                                        <input type="password" class="form-control" name="new_confirm_password" id="new_confirm_password" />
                                    </div>
                                </div>
                                <button id="tombol_simpan_password" onclick="handle_save('#tombol_simpan_password','#form_password','{{route('web.profile.cpassword')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Ubah Kata Sandi</button>
                                {{-- <button id="tombol_save" onclick="handle_save('#tombol_save','#password_form','{{route('office.profile.cpassword')}}','POST')" class="btn btn-primary" >Save Changes</button> --}}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-3">
                    <h5 class="me-3 mt-3">Keuangan</h5>
                    <div class="card">
                        <div class="card-body">
                            <form id="form_keuangan">
                                <div class="form-group row">
                                    <div class="col-4">
                                        <label>No Rekening</label>
                                        <input type="text" class="form-control" name="no_rek" id="no_rek" />
                                    </div>
                                    <div class="col-4">
                                        <label>Nama Pemilik Rekening</label>
                                        <input type="text" class="form-control" name="nama_rek" id="nama_rek" />
                                    </div>
                                    <div class="col-4">
                                        <label>Nama Bank</label>
                                       <select  class="form-control" name="bank" id="bank">
                                           <option value="bca">BCA</option>
                                           <option value="mandiri">MANDIRI</option>
                                           <option value="bni">BNI</option>
                                           <option value="bri">BRI</option>
                                       </select>
                                    </div>
                                </div>
                                <button id="tombol_simpan_bank" onclick="handle_save('#tombol_simpan_bank','#form_keuangan','{{route('web.profile.cbank')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Ubah Data Bank</button>
                                {{-- <button id="tombol_save" onclick="handle_save('#tombol_save','#password_form','{{route('office.profile.cpassword')}}','POST')" class="btn btn-primary" >Save Changes</button> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    number_only('no_rek');
</script>