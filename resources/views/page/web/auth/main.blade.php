<x-auth-layout title="Masuk">
    <div class="d-flex flex-column-fluid flex-center">
        <!--begin::Signin-->
        <div class="login-form login-signin">
            <!--begin::Form-->
            <form class="form" novalidate="novalidate" id="kt_login_signin_form">
                <!--begin::Title-->
                <div class="pb-13 pt-lg-0 pt-5">
                    <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Welcome to {{config('app.name')}}</h3>
                    <span class="text-muted font-weight-bold font-size-h4">
                        New Here?
                        <a href="javascript:;" id="kt_login_signup" class="text-primary font-weight-bolder">
                            Create an Account
                        </a>
                    </span>
                </div>
                <!--begin::Title-->
                <!--begin::Form group-->
                <div class="form-group">
                    <label class="font-size-h6 font-weight-bolder text-dark">Email</label>
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="email" id="email" name="email" autocomplete="off" />
                </div>
                <!--end::Form group-->
                <!--begin::Form group-->
                <div class="form-group">
                    <div class="d-flex justify-content-between mt-n5">
                        <label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
                        <a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Forgot Password ?</a>
                    </div>
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="password" id="password" name="password" autocomplete="off" />
                </div>
                <!--end::Form group-->
                <!--begin::Action-->
                <div class="pb-lg-0 pb-5">
                    <button type="button" id="kt_login_signin_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Sign In</button>
                </div>
                <!--end::Action-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Signin-->
        <!--begin::Signup-->
        <div class="login-form login-signup">
            <!--begin::Form-->
            <form class="form" novalidate="novalidate" id="kt_login_signup_form">
                <!--begin::Title-->
                <div class="pb-13 pt-lg-0 pt-5">
                    <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Sign Up</h3>
                    <p class="text-muted font-weight-bold font-size-h4">Enter your details to create your account</p>
                </div>
                <!--end::Title-->
                <!--begin::Form group-->
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="text" placeholder="Fullname" name="name" autocomplete="off" />
                </div>
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="tel" placeholder="Phone Number" name="phone" autocomplete="off" />
                </div>
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
                </div>
                <div class="form-group">
                    <textarea class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" placeholder="Address" name="address" autocomplete="off"></textarea>
                </div>
                <div class="form-group">
                    <select class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" name="role">
                        <option SELECTED DISABLED>Pilih Role</option>
                        <option value="2">Penyedia</option>
                        <option value="1">Penyewa</option>
                    </select>
                    {{-- <label>Foto KTP</label> --}}
                    {{-- <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="file" placeholder="KTP" name="ktp" autocomplete="off" /> --}}
                </div>
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="password" placeholder="Password" name="password" autocomplete="off" />
                </div>
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="password" placeholder="Confirm password" name="cpassword" autocomplete="off" />
                </div>
                <div class="form-group">
                    <label class="checkbox mb-0">
                        <input type="checkbox" name="agree" />
                        <span></span>
                        <div class="ml-2">I Agree the
                        <a href="javascript:;">terms and conditions</a>.</div>
                    </label>
                </div>
                <div class="form-group d-flex flex-wrap pb-lg-0 pb-3">
                    <button id="kt_login_signup_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
                    <button type="button" id="kt_login_signup_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Cancel</button>
                </div>
                <!--end::Form group-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Signup-->
        <!--begin::Forgot-->
        <div class="login-form login-forgot">
            <!--begin::Form-->
            <form class="form" novalidate="novalidate" id="kt_login_forgot_form">
                <!--begin::Title-->
                <div class="pb-13 pt-lg-0 pt-5">
                    <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Forgotten Password ?</h3>
                    <p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your password</p>
                </div>
                <!--end::Title-->
                <!--begin::Form group-->
                <div class="form-group">
                    <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" id="f_email" name="email" autocomplete="off" />
                </div>
                <!--end::Form group-->
                <!--begin::Form group-->
                <div class="form-group d-flex flex-wrap pb-lg-0">
                    <button type="button" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
                    <button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Cancel</button>
                </div>
                <!--end::Form group-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Forgot-->
    </div>
</x-auth-layout>