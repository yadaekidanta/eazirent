<x-web-layout title="Daftar Kendaraan">
    <div id="content_list">
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('{{asset('semicolon/demos/car/images/hero-slider/4.jpg')}}'); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
        <div class="container clearfix">
            <h1>Daftar Kendaraan</h1>
            {{-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:;">Daftar Kendaraan</a></li>
            </ol> --}}
        </div>
    </section>
    <section id="content">
        <div class="content-wrap pt-0" style="overflow: visible;">
                <div class="container">
                    <div class="card p-4 shadow" style="top: -60px;">
                        <form id="content_filter" class="mb-0">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-12 mt-4 mt-md-0">
                                    <label>All Brands</label>
                                    <select name="keywords[]" class="selectpicker customjs" title="Select Brand"  data-size="10" data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                        <optgroup label="All Brands">
                                            @foreach($brand as $brand)
                                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                {{-- <div class="col-md-6 col-sm-6 col-12 mt-4 mt-md-0">
                                    <label>Select Model</label>
                                    <select class="selectpicker customjs form-select" data-size="10" data-live-search="true" title="Select Model" style="width:100%; line-height: 30px;">
                                        <optgroup label="AUDI">
                                            <option value="R8">Audi R8</option>
                                            <option value="TT">Audi TT</option>
                                            <option value="S5">Audi S5</option>
                                            <option value="A5">Audi A5</option>
                                            <option value="TTS">Audi TTS</option>
                                            <option value="RS5">Audi RS 5</option>
                                        </optgroup>
                                        <optgroup label="BMW All Series">
                                            <option value="1-Series">BMW 1 Series 5-Door</option>
                                            <option value="Series-ActiveE">BMW 1 Series ActiveE</option>
                                            <option value="3-Series-Sedan">BMW 3 Series Sedan</option>
                                            <option value="ActiveHybrid-3">BMW ActiveHybrid 3</option>
                                            <option value="5-Series-Sedan">BMW 5 Series Sedan</option>
                                            <option value="ActiveHybrid-5">BMW ActiveHybrid 5</option>
                                            <option value="7-Series">BMW 7 Series</option>
                                            <option value="ActiveHybrid-7">BMW ActiveHybrid 7</option>
                                            <option value="M3-Sedan">BMW M3 Sedan</option>
                                            <option value="M5-Sedan">BMW M5 Sedan</option>
                                            <option value="3-Series-Turismo">BMW 3 Series Gran Turismo</option>
                                            <option value="5=Series-Turismo">BMW 5 Series Gran Turismo</option>
                                        </optgroup>
                                        <optgroup label="HONDA">
                                            <option value="Fit">Honda Fit</option>
                                            <option value="City">Honda City</option>
                                            <option value="Civic">Honda Civic</option>
                                            <option value="Fit-EV1">Honda Fit EV1</option>
                                            <option value="Accord">Honda Accord</option>
                                            <option value="Crosstour">Honda Crosstour</option>
                                            <option value="FCX-Clarity">Honda FCX Clarity</option>
                                            <option value="Civic-Hybrid">Honda Civic Hybrid</option>
                                            <option value="Accord-Hybrid">Honda Accord Hybrid</option>
                                            <option value="Accord-Plug-In">Honda Accord Plug-In</option>
                                        </optgroup>
                                        <optgroup label="MERCEDES-BENZ">
                                            <option value="S-Class">2021 Mercedes-Benz S-Class Sedan</option>
                                            <option value="C-Class">2021 Mercedes-Benz C-Class Sedan</option>
                                            <option value="E-Class">2021 Mercedes-Benz E-Class Sedan</option>
                                            <option value="E-Class-Hybrid">2021 Mercedes-Benz E-Class Hybrid</option>
                                            <option value="Maybach-S600">2021 Mercedes-Benz Maybach S600</option>
                                            <option value="B-Class-Electric-Drive">2021 Mercedes-Benz B-Class Electric Drive</option>
                                        </optgroup>
                                        <optgroup label="Ferrari">
                                            <option value="Ferrari Daytona">Ferrari Daytona</option>
                                            <option value="Ferrari 250 GTO">Ferrari 250 GTO</option>
                                            <option value="Ferrari 275">Ferrari 275</option>
                                            <option value="Ferrari 599 GTB Fiorano">Ferrari 599 GTB Fiorano</option>
                                            <option value="Ferrari F430">Ferrari F430</option>
                                            <option value="Ferrari 250">Ferrari 250</option>
                                        </optgroup>
                                        <optgroup label="Porsche">
                                            <option value="Porsche Carrera GT">Porsche Carrera GT</option>
                                            <option value="Porsche Boxster">Porsche Boxster</option>
                                            <option value="Porsche 911 classic">Porsche 911 classic</option>
                                            <option value="Porsche 911">Porsche 911</option>
                                            <option value="Porsche Cayman">Porsche Cayman</option>
                                            <option value="Porsche Panamera">Porsche Panamera</option>
                                            <option value="Porsche 959">Porsche 959</option>
                                            <option value="Porsche 356">Porsche 356</option>
                                            <option value="Porsche Cayenne">Porsche Cayenne</option>
                                            <option value="Porsche 997">Porsche 997</option>
                                        </optgroup>
                                    </select>
                                </div>  --}}
                                <div class="col-md-2 col-sm-6 col-6">
                                    <button type="button" onclick="load_list(1);" class="button button-3d button-rounded w-100 ms-0" style="margin-top: 29px;">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="section m-0 pt-0 bg-transparent">
                    <div class="container">
                        <!-- Grid Filter ============================================= -->
                        <!-- .grid-filter end -->
                        <!-- Portfolio Items ============================================= -->
                        <div id="list_result"></div>
                    </div>
                </div>
                <div class="section center nbottomomargin mt-3 footer-stick" style="padding:80px 0 ">
                    <div class="container clearfix">
                        <h3 class="font-primary">Special Offers on Villa Long Term Rentals &amp; Lease Agreements</h3>
                        <a href="contact.html" class="button button-color button-large button-rounded">Contact Now</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-web-layout>