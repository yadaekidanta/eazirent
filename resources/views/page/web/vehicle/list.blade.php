<div id="portfolio" class="portfolio row gutter-30 grid-container" data-layout="fitRows">
    <!-- Car 1 -->
@foreach ($collection as $item)
<article class="portfolio-item col-12 col-sm-6 col-md-4 cf-sedan">
    <div class="grid-inner">
        <div class="portfolio-image">
            <a href="#">
                <img src="{{$item->image}}" alt="{{$item->brand->name}}">
                <div class="filter-p-pricing">
                    <span class="p-price fw-bold ls1">Rp {{number_format($item->price)}} / hari</span>
                    {{-- <span class="p-price-msrp">MSRP : <strong>{{$item->price}}</strong></span> --}}
                </div>
            </a>
        </div>
        <div class="portfolio-desc">
            <h3><a href="#">{{$item->brand->name}} - {{$item->vehicleModel->name}}</a></h3>
            <span>{{$item->desc}} </span>
            <span>{{$item->owner->name}}</span>
        </div>
        @if(Auth::guard('member')->user()->role=="1")
        <div class="row g-0 car-p-features font-primary clearfix">
            <div class="col-lg-4 col-6 p-0"><a href="javascript:;" onclick="load_input('{{route('web.order.create',$item->id)}}');" class="button button-rounded button-small m-0 ms-2 float-end">Pesan Kendaraan</a></div>
        </div>
        @endif
    </div>
</article> 
@endforeach
</div>