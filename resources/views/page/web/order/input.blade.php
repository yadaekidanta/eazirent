<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <a href="javascript:;" onclick="load_list(1);" class="button button-rounded button-small m-0 ms-2 float-end">Kembali</a>
                    <div class="heading-block border-0">
                        <h3>{{$vehicle->owner->name}}</h3>
                    </div>
                    <div class="clear"></div>
                    <h5 class="me-3 mt-3">Data Pengusaha</h5>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Nama Pemilik : </th>
                                <th>{{$vehicle->owner->name}}</th>
                            </tr>
                            <tr>
                                <th>No Telp Pemilik : </th>
                                <th>{{$vehicle->owner->phone}}</th>
                            </tr>
                            <tr>
                                <th>Email Pemilik : </th>
                                <th>{{$vehicle->owner->email}}</th>
                            </tr>
                            <tr>
                                <th>Alamat Pemilik : </th>
                                <th>{{$vehicle->owner->address}}</th>
                            </tr>
                            <tr>
                                <th>Nama Bank : </th>
                                <th>{{$vehicle->owner->nama_rek}}</th>
                            </tr>
                            <tr>
                                <th>No rek Pemilik : </th>
                                <th>{{$vehicle->owner->no_rek}}</th>
                            </tr>
                        </table>
                    <div class="clear"></div>
                    <h5 class="me-3 mt-3">Data Kendaraan</h5>
                    <div class="card">
                        <div class="card-body">
                            <form id="form_basic_info">
                                <div class="form-group row">
                                    <div class="col-3">
                                        <input type="hidden" name="penyedia_id" id="penyedia_id" value="{{$vehicle->owner->id}}">
                                        <label>Vehicle</label>
                                        <input type="text" readonly name="vehicle" class="form-control" placeholder="Full" value="{{$vehicle->brand->name}} - {{$vehicle->vehicleModel->name}}" />
                                        <input type="hidden" readonly name="vehicle_id" class="form-control" placeholder="Full" value="{{$vehicle->id}}" />
                                    </div>
                                    <div class="col-3">
                                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                        <input type="text" class="form-control" autocomplete="off" id="tanggal" name="booking_at" placeholder="Masukkan Tanggal..." aria-label="First name">
                                    </div>
                                    <div class="col-3">
                                        <label class="required fs-6 fw-bold mb-2">Jam Sewa</label>
                                        <div class="input-group text-start" data-target-input="nearest" data-target=".datetimepicker1">
                                            <input type="text" name="jsewa" class="form-control datetimepicker-input datetimepicker1" data-target=".datetimepicker1" placeholder="00:00 AM/PM" />
                                            <div class="input-group-text"  data-target=".datetimepicker1" data-toggle="datetimepicker"><i class="icon-clock"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label>Jumlah hari</label>
                                        <input type="text" id="days" name="days" class="form-control" placeholder="Masukkan lama booking..."/>
                                    </div>
                                    <input type="hidden" id="harga" value="{{$vehicle->price}}">
                                    <div class="col-3">
                                        <label>Total Harga</label>
                                        <input type="text" id="total" name="total" class="form-control" readonly />
                                    </div>
                                    <div class="col-6">
                                        <label>Cara pembayaran:</label>
                                        <select data-control="select2" data-placeholder="Pilih Cara pembayaran" id="payment_type" name="payment_type" class="form-control">
                                            <option SELECTED DISABLED>Pilih Cara pembayaran</option>
                                            <option value="cod">COD</option>
                                            <option value="tf">Transfer</option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <label>Bukti Transfer</label>
                                        <input type="file" id="btf" name="btf" class="form-control"/>
                                    </div>
                                </div>
                                <button id="tombol_simpan_basic" onclick="handle_upload('#tombol_simpan_basic','#form_basic_info','{{route('web.order.store')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Pesan sekarang</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    datepicker_start('tanggal');
    $('#days').on('keyup', function() {
                var harga = $("#harga").val();
                // harga = harga.replace(',','');
                var total = harga * this.value;
                $("#total").val(format_ribuan(total));
            });
        
        $("#btf").hide();

        $('#payment_type').change(function(){
            if(this.value == "tf"){
                $("#btf").show();
            }else{
                $("#btf").hide();
            }
        })
        number_only("days");
        $('#jsewa').datetimepicker({
            format: 'LT',
            showClose: true
        });
        $('.datetimepicker').datetimepicker({
				showClose: true
			});
        $('.datetimepicker1').datetimepicker({
            format: 'LT',
            showClose: true
        });
</script>
