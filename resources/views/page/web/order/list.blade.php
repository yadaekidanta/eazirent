<x-web-layout title="Transaksi">
    <div id="content_list">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="tabs tabs-alt clearfix" id="tabs-profile">
                            <ul class="tab-nav clearfix">
                                <li><a href="#tab-transaksi"><i class="icon-rss2"></i> Transaksi</a></li>
                                @if (Auth::guard('member')->user()->role == 2)
                                <li><a href="#tab-mobil"><i class="icon-rss2"></i>Kendaraan</a></li>
                                @endif
                            </ul>
                            <div class="tab-container">
                                <div class="tab-content clearfix" id="tab-transaksi">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                                <tr>
                                                    <th>Tanggal</th>
                                                    <th>Waktu</th>
                                                    <th>Jumlah Hari</th>
                                                    <th>Kendaraan</th>
                                                    <th>Nama Penyewa</th>
                                                    <th>No HP Penyewa</th>
                                                    <th>KTP Penyewa</th>
                                                    <th>Jenis Pembayaran</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order as $item)
                                                <tr>
                                                    <td>
                                                        <code>{{$item->booking_at->format('j F Y')}}</code>
                                                    </td>
                                                    <td>{{$item->booking_time}}</td>
                                                    <td>{{$item->days}}</td>
                                                    <td>{{$item->vehicle->brand->name}} - {{$item->vehicle->vehicleModel->name}}</td>
                                                    <td>{{$item->user->name}}</td>
                                                    <td>{{$item->user->phone}}</td>
                                                    @php
                                                        $payment = "";
                                                        if($item->payment_type == "tf"){
                                                            $payment = "Transfer";
                                                        }else{
                                                            $payment =  "COD";
                                                        }
                                                        @endphp
                                                    <td><img src="{{asset($item->user->ktp)}}"></td>
                                                    <td>{{$payment}}</td>
                                                    <td>{{$item->st}}</td>
                                                    <td>
                                                        @if($item->file)
                                                            <a href="{{route('web.order.download',$item->id)}}" target="_blank">Download Bukti</a>
                                                        @endif
                                                        {{-- @if(Auth::guard("member")->user()->role == "1")
                                                            @if($item->st == "Rent finish")
                                                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('web.order.ongoing',$item->id)}}');">Review</a>
                                                            @endif
                                                        @endif  --}}    
                                                        <br>
                                                        @if(Auth::guard("member")->user()->role != "1")
                                                        
                                                            @if($item->st == "Wait for confirmation")
                                                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('web.order.ongoing',$item->id)}}');">Penyerahan Kunci</a>
                                                                <br>
                                                            @elseif($item->st == "Rent on going")
                                                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('web.order.finish',$item->id)}}');">Pengembalian Kunci</a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-content clearfix" id="tab-mobil">
                                    <a href="javascript:;" onclick="load_input('{{route('web.vehicle.create')}}');" class="button button-rounded button-small m-0 ms-2 float-end">Tambah kendaraan</a>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Brand</th>
                                                    <th>Kategori</th>
                                                    <th>Vehicle model</th>
                                                    <th>Deskripsi</th>
                                                    <th>Aksi</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no = 1;
                                            @endphp
                                            @foreach ($mobil as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$item->brand->name}}</td>
                                                <td>{{$item->category->name}}</td>
                                                <td>{{$item->vehicleModel->name}}</td>
                                                <td>{{$item->desc}}</td>
                                                <td>
                                                    <a href="javascript:;" onclick="load_input('{{route('web.vehicle.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-circle mr-2">
                                                        <i class="flaticon-file">Edit</i>
                                                    </a>
                                                    <a href="javascript:;" onclick="handle_confirm('Delete Confirmation','Yes','Cancel','DELETE','{{route('web.vehicle.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-circle mr-2">
                                                        <i class="flaticon-file">Delete</i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>

        </script>
    @endsection
</x-web-layout>