<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\ProfileController;
use App\Http\Controllers\Web\OrderController;
use App\Http\Controllers\Web\VehicleController;

Route::group(['domain' => ''], function() {
    Route::name('web.')->group(function(){
        Route::get('', [WebController::class, 'index'])->name('home');
        Route::resource('vehicle', VehicleController::class);
        Route::get('vehicle/{vehicle}/order', [OrderController::class, 'create'])->name('order.create');
        Route::post('order/store', [OrderController::class, 'store'])->name('order.store');
        Route::get('my-vehicle', [VehicleController::class, 'list'])->name('vehicle.list');
        Route::get('contact', [WebController::class, 'contact'])->name('contact');
        Route::post('contact/store', [WebController::class, 'do_contact'])->name('contact.store');
        Route::get('auth', [AuthController::class, 'index'])->name('auth.index');
        Route::get('order/show', [OrderController::class, 'show'])->name('order.show');
        Route::post('order/{order}/ongoing', [OrderController::class, 'ongoing'])->name('order.ongoing');
        Route::post('order/{order}/finish', [OrderController::class, 'finish'])->name('order.finish');
        Route::post('auth/login', [AuthController::class, 'do_login'])->name('auth.login');
        Route::post('auth/register', [AuthController::class, 'do_register'])->name('auth.register');
        Route::get('account/verify/{token}', [AuthController::class, 'verify'])->name('verify'); 
        Route::get('order/{order}/download', [OrderController::class, 'download'])->name('order.download');
        Route::middleware(['auth:member'])->group(function(){
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('', [ProfileController::class, 'index'])->name('index');
                Route::get('edit', [ProfileController::class, 'edit'])->name('edit');
                Route::post('cpassword', [ProfileController::class, 'update_password'])->name('cpassword');
                Route::post('cbank', [ProfileController::class, 'update_bank'])->name('cbank');
                Route::post('update', [ProfileController::class, 'update'])->name('update');
            });
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
    });
});