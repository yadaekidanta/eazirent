<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Office\AuthController;
use App\Http\Controllers\Master\BrandController;
use App\Http\Controllers\Master\VehicleController;
use App\Http\Controllers\Master\CategoryController;
use App\Http\Controllers\Office\DashboardController;
use App\Http\Controllers\Master\VehicleModelController;
use App\Http\Controllers\Office\BannerController;
use App\Http\Controllers\Office\OrderController;
use App\Http\Controllers\Office\ContactController;
use App\Http\Controllers\Office\EmployeeController;
use App\Http\Controllers\Office\MemberController;
use App\Http\Controllers\Office\RentController;

Route::group(['domain' => ''], function() {
    Route::prefix('office')->name('office.')->group(function(){
        Route::get('auth',[AuthController::class, 'index'])->name('auth.index');
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('password_changed',[AuthController::class, 'password_changed'])->name('password_changed');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
            Route::get('verify/{auth:email}',[AuthController::class, 'do_verify'])->name('verify');
        }); 
        Route::middleware(['auth:office'])->group(function(){
            Route::redirect('/', 'office/dashboard', 301);
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::resource('category', CategoryController::class);
            Route::resource('banner', BannerController::class);
            Route::resource('contact', ContactController::class);
            Route::resource('brand', BrandController::class);
            Route::resource('vehicleModel', VehicleModelController::class);
            Route::resource('vehicle', VehicleController::class);
            Route::resource('member', MemberController::class);
            Route::resource('rent', RentController::class);
            Route::resource('employee', EmployeeController::class);
            Route::resource('contact', ContactController::class);
            Route::resource('banner', BannerController::class);
            Route::resource('order', OrderController::class);
            Route::post('member/{member}/verif',[MemberController::class, 'verif'])->name('member.verif');
            Route::post('rent/{rent}/verif',[RentController::class, 'verif'])->name('rent.verif');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('', [ProfileController::class, 'index'])->name('index');
                Route::get('edit', [ProfileController::class, 'edit'])->name('edit');
                Route::post('cpassword', [ProfileController::class, 'cpassword'])->name('cpassword');
                Route::post('save', [ProfileController::class, 'save'])->name('save');
            });

            Route::get('storage-link', function(){
                Artisan::call('storage:link');
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Storage Linked!'
                ]);
            })->name('storage.link');
        });
    });
});