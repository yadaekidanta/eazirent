<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    use HasFactory;
    protected $table = 'vehicle_model';
    public $timestamps = false;
    public function brand(){
        return $this->belongsTo(Brand::class,'id','brand_id');
    }
}
