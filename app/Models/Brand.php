<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $table = 'brand';
    public $timestamps = false;
    public function category(){
        return $this->belongsTo(Category::class,'id','category_id');
    }
}
