<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'order';
    public $timestamps = false;
    protected $casts = [
        'booking_at' => 'date',
        'take_at' => 'datetime',
        'return_at' => 'datetime',
        'created_at' => 'datetime',
    ];
    public function getImageAttribute()
    {
        return asset('storage/' . $this->photo);
    }
    public function vehicle(){
        return $this->belongsTo(Vehicle::class,'vehicle_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
