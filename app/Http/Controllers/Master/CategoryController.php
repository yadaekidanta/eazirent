<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Category::where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.category.list', compact('collection'));
        }
        return view('page.office.category.main');
    }
    public function create()
    {
        return view('page.office.category.input', ['data' => new Category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $category = new Category;
        $category->name = Str::title($request->name);
        $category->slug = Str::slug($request->name);
        $category->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Category '. $request->name . ' tersimpan',
        ]);
    }
    public function show(Category $category)
    {
        //
    }
    public function edit(Category $category)
    {
        return view('page.office.category.input', ['data' => $category]);
    }
    public function update(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $category->name = Str::title($request->name);
        $category->slug = Str::slug($request->name);
        $category->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Category '. $request->name . ' tersimpan',
        ]);
    }
    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Category '. $category->name . ' terhapus',
        ]);
    }
}