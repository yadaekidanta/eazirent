<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Brand;     
use App\Models\Category;     
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Brand::where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.brand.list', compact('collection'));
        }
        return view('page.office.brand.main');
    }
    public function create()
    {
        $category = Category::get();
        return view('page.office.brand.input', ['brand' => new Brand,'category'=>$category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }
        }
        $brand = new Brand;
        $brand->name = Str::title($request->name);
        $brand->slug = Str::slug($request->name);
        $brand->category_id = $request->category_id; 
        $brand->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand '. $request->name . ' tersimpan',
        ]);
    }
    public function show(Brand $brand)
    {
        //
    }
    public function edit(Brand $brand)
    {
        $category = Category::get();
        return view('page.office.brand.input', ['brand' => $brand,'category'=>$category]);
    }
    public function update(Request $request, Brand $brand)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }
        }
        $brand->name = Str::title($request->name);
        $brand->slug = Str::slug($request->name); 
        $brand->category_id = $request->category_id; 
        $brand->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand '. $request->name . ' tersimpan',
        ]);
    }
    public function destroy(Brand $brand)
    {
        $brand->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Brand '. $brand->name . ' terhapus',
        ]);
    }
}
