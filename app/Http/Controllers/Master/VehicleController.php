<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Models\VehicleModel;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Vehicle::where('desc','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.vehicle.list',compact('collection'));
        }
        return view('page.office.vehicle.main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand=Brand::get();
        $vehicleModel=VehicleModel::get();
        $category=Category::get();
        return view('page.office.vehicle.input', ['brand'=>$brand,'vehicle' => new Vehicle, 'category'=>$category,'vehicleModel'=>$vehicleModel]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'category_id' => 'required',
            'vehicle_model_id' => 'required',
            'desc' => 'required',
            'photo' => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }else if ($errors->has('category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }else if ($errors->has('vehicle_model_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('vehicle_model_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $file = request()->file('photo')->store("vehicle");
        $vehicle = new Vehicle;
        $vehicle->brand_id = $request->brand_id;
        $vehicle->user_id = 0;
        $vehicle->category_id = $request->category_id;
        $vehicle->vehicle_model_id = $request->vehicle_model_id;
        $vehicle->desc = $request->desc;
        $vehicle->price = Str::remove(',', $request->price);
        $vehicle->photo = $file;
        $vehicle->st = "open";
        $vehicle->created_at = date('Y-m-d H:i:s');
        $vehicle->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle '. $request->desc . ' tersimpan',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('page.web.vehicle.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $product)
    {   
        $brand=Brand::get();
        $vehicleModel=VehicleModel::get();
        $category=Category::get();
        return view('page.office.vehicle.input', ['brand'=>$brand,'vehicle' => new Vehicle, 'category'=>$category,'vehicleModel'=>$vehicleModel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'category_id' => 'required',
            'vehicle_model_id' => 'required',
            'desc' => 'required',
            'price' => 'required',
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }else if ($errors->has('category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }else if ($errors->has('vehicle_model_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('vehicle_model_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($vehicle->photo);
            $photo = request()->file('photo')->store("productVarian");
            $vehicle->photo = $photo;
        }
        $vehicle->brand_id = $request->brand_id;
        $vehicle->user_id = 0;
        $vehicle->category_id = $request->category_id;
        $vehicle->vehicle_model_id = $request->vehicle_model_id;
        $vehicle->desc = $request->desc;
        $vehicle->price = Str::remove(',', $request->price);
        $vehicle->st = "open";
        $vehicle->created_at = date('Y-m-d H:i:s');
        $vehicle->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle '. $request->desc . ' terupdate',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle '. $vehicle->desc . ' terhapus',
        ]);
    }

}
