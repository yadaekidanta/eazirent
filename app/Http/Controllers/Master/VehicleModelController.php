<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\VehicleModel;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;


class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = VehicleModel::where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.vehicleModel.list', compact('collection'));
        }
        return view('page.office.vehicleModel.main');
    }
    public function create()
    {
        $brand = Brand::get();
        return view('page.office.vehicleModel.input', ['vehicleModel' => new VehicleModel,'brand'=>$brand]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'brand_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }
        }
        $vehicleModel = new VehicleModel;
        $vehicleModel->name = Str::title($request->name);
        $vehicleModel->slug = Str::slug($request->name);
        $vehicleModel->brand_id = $request->brand_id; 
        $vehicleModel->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'VehicleModel '. $request->name . ' tersimpan',
        ]);
    }
    public function show(VehicleModel $vehicleModel)
    {
        //
    }
    public function edit(VehicleModel $vehicleModel)
    {
        $brand = Brand::get();
        return view('page.office.vehicleModel.input', ['vehicleModel' => $vehicleModel,'brand'=>$brand]);
    }
    public function update(Request $request, VehicleModel $vehicleModel)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'brand_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }
        }
        $vehicleModel->name = Str::title($request->name);
        $vehicleModel->slug = Str::slug($request->name); 
        $vehicleModel->brand_id = $request->brand_id; 
        $vehicleModel->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'VehicleModel '. $request->name . ' tersimpan',
        ]);
    }
    public function destroy(VehicleModel $vehicleModel)
    {
        $vehicleModel->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'VehicleModel '. $vehicleModel->name . ' terhapus',
        ]);
    }
}
