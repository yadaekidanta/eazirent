<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Banner::where('title','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.banner.list', compact('collection'));
        }
        return view('page.office.banner.main');
    }
    public function create()
    {
        return view('page.office.banner.input', ['banner' => new Banner]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'desc' => 'required',
            'url' => 'required',
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('url')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('url'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('photo')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $data = new Banner;
        if(request()->file('photo')){
            $photo = request()->file('photo')->store("banner");
            $data->photo = $photo;
        }
        $data->title = Str::title($request->title);
        $data->desc = $request->desc;
        $data->url = $request->url;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Banner '. $request->title . ' tersimpan',
        ]);
    }
    public function show(Banner $banner)
    {
        //
    }
    public function edit(Banner $banner)
    {
        return view('page.office.banner.input', compact('banner'));
    }
    public function update(Request $request, Banner $banner)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'desc' => 'required',
            'url' => 'required',
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('url')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('url'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('photo')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($banner->company_logo);
            $photo = request()->file('photo')->store("banner");
            $banner->photo = $photo;
        }
        $banner->title = Str::title($request->title);
        $banner->desc = $request->desc;
        $banner->url = $request->url;
        $banner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Banner '. $request->title . ' terupdate',
        ]);
    }
    public function destroy(Banner $banner)
    {
        Storage::delete($banner->photo);
        $banner->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Banner terhapus',
        ]);
    }
}
