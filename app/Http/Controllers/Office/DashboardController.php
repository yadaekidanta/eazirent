<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Models\Vehicle;

class DashboardController extends Controller
{
    public function index()
    {
        $vehicle = Vehicle::get()->count();
        $order = Order::get()->count();
        $member = User::where("email_verified_at","!=",null)->count();
        return view('page.office.dashboard.main',compact('vehicle', 'order', 'member'));
    }
}
