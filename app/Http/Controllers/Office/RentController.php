<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\User;     
use App\Models\Category;     
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = User::where('name','like','%'.$keywords.'%')->where('role','1')
            ->paginate(10);
            return view('page.office.rent.list', compact('collection'));
        }
        return view('page.office.rent.main');
    }
    public function create()
    {
       //
    }
    public function store(Request $request)
    {
       //
    }
    public function show(User $member)
    {
        //
    }
    public function edit(User $member)
    {
        //
    }
    public function update(Request $request, User $member)
    {
       //
    }
    public function destroy(User $rent)
    {
       //
    }
    public function verif(User $rent){
        $rent->email_verified_at =  date('Y-m-d H:i:s');
        $rent->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'User '. $rent->name . ' terverifikasi',
        ]);
    }
}
