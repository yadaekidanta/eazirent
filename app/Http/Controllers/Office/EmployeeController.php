<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Employee::where('name','like','%'.$keywords.'%')->paginate(10);
            return view('page.office.employee.list', compact('collection'));
        }
        return view('page.office.employee.main');
    }
    public function create()
    {
        return view('page.office.employee.input', ['employee' => new Employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        $employee= new Employee;
        $employee->name = Str::title($request->name);
        $employee->email = $request->email;
        $employee->email_verified_at = date('Y-m-d H:i:s');
        $employee->created_at = date('Y-m-d H:i:s');
        $employee->updated_at = date('Y-m-d H:i:s');
        $employee->password = Hash::make($request->password);
        $employee->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee '. $request->name . ' tersimpan',
        ]);
    }
    public function show(Employee $employee)
    {
        //
    }
    public function edit(Employee $employee)
    {
        //
    }
    public function update(Request $request, Employee $employee)
    {
        //
    }
    public function destroy(Employee $employee)
    {
        $employee->forceDelete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Banner terhapus',
        ]);
    }
}
