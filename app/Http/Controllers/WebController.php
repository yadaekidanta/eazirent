<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{
    public function index()
    {
        return view('page.web.home.main');
    }
    public function do_contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('message')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('message'),
                ]);
            }
        }
        
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->created_at = date("Y-m-d H:i:s");
        $contact->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Thanks for contact us',
        ]);
    }
}
