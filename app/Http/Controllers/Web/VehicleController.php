<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Category;
use App\Models\VehicleModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    public function index(Request $request)
    {
        $brand = Brand::get();
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Vehicle::whereIn('brand_id',$keywords)
            ->where('st','!=','used')
            ->paginate(10);
            return view('page.web.vehicle.list', compact('collection'));
        }
        return view('page.web.vehicle.main',compact('brand'));
    }
    public function list()
    {
        $collection = Vehicle::where('user_id', Auth::guard('member')->user()->id)->get();
        return view('page.web.vehicle.main',compact('collection'));
    }
    public function create()
    {
        $brand=Brand::get();
        $vehicleModel=VehicleModel::get();
        $category=Category::get();
        return view('page.web.profile.input_vehicle',['brand'=>$brand,'vehicle' => new Vehicle, 'category'=>$category,'vehicleModel'=>$vehicleModel]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'category_id' => 'required',
            'vehicle_model_id' => 'required',
            'desc' => 'required',
            'photo' => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }else if ($errors->has('category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }else if ($errors->has('vehicle_model_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('vehicle_model_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $file = request()->file('photo')->store("vehicle");
        $vehicle = new Vehicle;
        $vehicle->brand_id = $request->brand_id;
        $vehicle->user_id = Auth::guard('member')->user()->id;
        $vehicle->category_id = $request->category_id;
        $vehicle->vehicle_model_id = $request->vehicle_model_id;
        $vehicle->desc = $request->desc;
        $vehicle->price = Str::remove(',', $request->price);
        $vehicle->photo = $file;
        $vehicle->st = "open";
        $vehicle->created_at = date('Y-m-d H:i:s');
        $vehicle->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle tersimpan',
        ]);
    }
    public function edit(Vehicle $vehicle)
    {
        $brand=Brand::get();
        $vehicleModel=VehicleModel::get();
        $category=Category::get();
        return view('page.web.profile.input_vehicle',['brand'=>$brand,'vehicle' => $vehicle, 'category'=>$category,'vehicleModel'=>$vehicleModel]);
    }
    public function update(Request $request, Vehicle $vehicle)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'category_id' => 'required',
            'vehicle_model_id' => 'required',
            'desc' => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('brand_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('brand_id'),
                ]);
            }else if ($errors->has('category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_id'),
                ]);
            }else if ($errors->has('vehicle_model_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('vehicle_model_id'),
                ]);
            }else if ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }else if ($errors->has('price')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price'),
                ]);
            }
            else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        } 
        if(request()->file('photo')){
            Storage::delete($vehicle->photo);
            $photo = request()->file('photo')->store("productVarian");
            $vehicle->photo = $photo;
        }
        $vehicle->brand_id = $request->brand_id;
        $vehicle->user_id = Auth::guard('member')->user()->id;
        $vehicle->category_id = $request->category_id;
        $vehicle->vehicle_model_id = $request->vehicle_model_id;
        $vehicle->desc = $request->desc;
        $vehicle->price = Str::remove(',', $request->price);
        $vehicle->st = "open";
        $vehicle->created_at = date('Y-m-d H:i:s');
        $vehicle->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle '. $request->desc. ' terupdate',
        ]);
    }
    public function destroy(Vehicle $vehicle)
    {
        Order::where('vehicle_id',$vehicle->id)->delete();
        $vehicle->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vehicle  terhapus',
        ]);
    }
}