<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function create(Vehicle $vehicle)
    {
        return view('page.web.order.input', compact('vehicle'));
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vehicle_id' => 'required',
            'booking_at' => 'required',
            'booking_time' => 'required',
            'days' => 'required',
            'total' => 'required',
            'payment_type' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('vehicle_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('vehicle_id'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('booking_at')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('booking_at'),
                ]);
            }
        }
        elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('days')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('days'),
                ]);
            }
        }elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('total')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('total'),
                ]);
            }
        }elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('payment_type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('payment_type'),
                ]);
            }
        }elseif ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jsewa')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jsewa'),
                ]);
            }
        }
        $order = new Order;
        $order->user_id = Auth::guard('member')->user()->id;
        $order->penyedia_id = $request->penyedia_id;
        $order->vehicle_id = $request->vehicle_id;
        $order->booking_at = $request->booking_at;
        $order->booking_time = $request->jsewa;
        $order->days = $request->days;
        if(request()->file('btf')){
            $photo = request()->file('btf')->store("bukti");
            $order->file = $photo;
        }
        $order->total = $request->total;
        $order->st = "Wait for confirmation";
        $order->created_at = date('Y-m-d H:i:s');
        $order->payment_type = $request->payment_type;
        $order->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Order tersimpan',
        ]);
    }
    public function download(Order $order)
    {
        $extension = Str::of($order->file)->explode('.');
        return Storage::download($order->file, 'order-'.$order->created_at.'.'.$extension[1]);
    }

    public function show(Order $order)
    {
        $role = Auth::guard('member')->user()->role;
        if($role == 1){
            $order = Order::where("user_id",Auth::guard('member')->user()->id)->get();
        }else{
            $order = Order::where("penyedia_id",Auth::guard('member')->user()->id)->get();
        }
        $mobil = Vehicle::where('user_id', Auth::guard('member')->user()->id)->get();
        return view("page.web.order.list", compact('order','mobil'));
    }

    public function ongoing(Order $order)
    {
        
        $order = Order::where('id', $order->id)->first();
        $vehicle = Vehicle::where('id', $order->vehicle_id)->first();
        $order->st = "Rent on going";
        $order->take_at = date("Y-m-d H:i:s");
        $vehicle->st = "used";
        $order->update();
        $vehicle->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berhasil',
        ]);
    }
    public function finish(Order $order)
    {
        $order = Order::where('id', $order->id)->first();
        $vehicle = Vehicle::where('id', $order->vehicle_id)->first();
        $order->st = "Rent finish";
        $vehicle->st = "open";
        $order->return_at = date("Y-m-d H:i:s");
        $order->update();
        $vehicle->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berhasil',
        ]);
    }
    public function edit(Order $order)
    {
        //
    }
    public function update(Request $request, Order $order)
    {
        //
    }
    public function destroy(Order $order)
    {
        //
    }
}
