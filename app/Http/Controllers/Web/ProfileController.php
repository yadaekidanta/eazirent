<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Vehicle;
use App\Models\Order;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(Request $request)
    {   
        if (Auth::guard('member')->user()->role == 1){
            $order = Order::where('user_id', Auth::guard('member')->user()->id)->get();
        }else{
            $order = Order::get();
        }
        $mobil = Vehicle::where('user_id', Auth::guard('member')->user()->id)->get();
        $profile = User::where('id',Auth::guard('member')->user()->id)->first();
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Order::where('user_id',$profile->id)
            ->paginate(10);
            return view('page.web.profile.list', compact('collection'));
        }
        return view('page.web.profile.main',compact('profile','mobil','order'));
    }
    public function edit()
    {
        return view('page.web.profile.input');
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'name' => 'required',
            'nik'=>'required',
            'address'=>'required',
            'ktp'=>'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('nik')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nik'),
                ]);
            }else if ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ktp'),
                ]);
            }
        }
        $user = User::where('id', Auth::guard('member')->user()->id)->first();
        if(request()->file('ktp')){
            Storage::delete($user->ktp);
            $photo = request()->file('ktp')->store("profile");
            $user->ktp = $photo;
        }
        $user->name = Str::title($request->name);
        $user->email = $request->email;
        $user->nik = $request->nik;
        $user->address = $request->address;
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profile '. $request->name . ' terupdate',
        ]);
    }
    public function update_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('current_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('current_password'),
                ]);
            } elseif ($errors->has('new_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('new_password'),
                ]);
            } elseif ($errors->has('new_confirm_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('new_confirm_password'),
                ]);
            }
        }
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        $profile = User::where('id',Auth::guard('member')->user()->id)->first();
        // Mail::to($profile->email)->send(new PasswordChangedMail($profile));
        $profile = Auth::guard('member')->user();
        Auth::logout($profile);
        return response()->json([
            'alert' => 'success',
            'message' => 'Password changed successfully',
        ]);
    }
    public function update_bank(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_rek' => 'required',
            'nama_rek' => 'required',
            'bank' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('no_rek')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_rek'),
                ]);
            } elseif ($errors->has('nama_rek')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_rek'),
                ]);
            } elseif ($errors->has('bank')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank'),
                ]);
            }
        }
        $user = User::where('id',Auth::guard('member')->user()->id)->first();
        $user->no_rek = $request->no_rek;
        $user->nama_rek = $request->nama_rek;
        $user->bank = $request->bank;
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Bank account successfully',
        ]);
    }
}